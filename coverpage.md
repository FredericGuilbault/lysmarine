
# Lysmarine
## A libre navigation OS for Raspberrypi
Lysmarine is a navigation computer for boats. It's designed for the raspberrypi and provide a stable, low-cost and expandable tool.

 [Download & install](install.md) [Documentation](README.md)
