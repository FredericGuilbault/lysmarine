# Once the develop branch of lysmarine is at your taste and you are ready do release





## Build lysmarine
 - CLean the work folder with: `rm -rf ./work`.
 - Change version number in the `config` file at line `IMG_DATE`.
 - Change the version number in the conky script file at `50-desktopEnvironment/01-theming/files/.conkyrc`.
 - Make a commit.
 - Merge into master.
 - Create a tag.
 - Build.

## Create the archive files
__todo__

## Upload them
__todo__

## Write the changelog.
__todo__
